int main(void) {
	
	// Die LED soll leuchten.
	// DAfuer brauchen wir DREI Schritte
	// Schritt 1: Port GPIOA mittels RCC (Clock Enable Register) betakten
	// Schritt 2: GPIOA.5 als AUSGANG konfigurieren (Mode Register)
	// Schritt 3: dort einen EINSER rausschicken (Output Data Register)
	
	// AH81CLKEHA -> 0x4002 3800 + 30
	// ***1*** das Register hat die Adresse 0x4002 3830
	// und darin soll das Bit NR0 auf 1 gesetzt werden (Enable GPIO A)
	*((unsigned long*)0x40023830) |= (1 << 0); // BIT NUMMER 0 AUF EINS SETZEN
	
	
	// ***2*** ModeRegister SO einstellen, dass der Pin A.5 ein AUSGANG wird...
	*((unsigned long*)0x40020000) |= (1<<10); // Bit 11 und 10 sind jetzt 01 ... OUTPUT
	
	
	// ***3*** OUTPUT DATA REGISTER muss einen 1er haben (ann Stelle 5, eh klar)
	*((unsigned long *)0x40020014) |= (1<<5); // Pin5 auf 1 setzen
	
	
	// Endlosschleife, denn sonst wuerde der Kern
	// die Arbeit einstellen und das ist eigentlich
	// nicht gewuenscht...
	while(1);
	
}
