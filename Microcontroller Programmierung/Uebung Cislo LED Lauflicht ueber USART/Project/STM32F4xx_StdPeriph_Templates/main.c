#include "stm32f4xx_conf.h"

int button_pressed = 0;
int change_direction = 0;
int current_led = 0;
uint16_t LEDs[4] = { GPIO_Pin_7, GPIO_Pin_8, GPIO_Pin_11, GPIO_Pin_12 };

void wait_ms(int ms) {
    int i;
    for (i=1; i<ms*20000; i++);    
}

int main(void) {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); 
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	GPIO_InitTypeDef GPIOA_Struct;
	NVIC_InitTypeDef NVIC_Struct;
	USART_InitTypeDef USART_Struct;
	EXTI_InitTypeDef EXTI_Struct;


	//GPIO
	GPIOA_Struct.GPIO_Mode=GPIO_Mode_AF;
	GPIOA_Struct.GPIO_OType=GPIO_OType_PP;
	GPIOA_Struct.GPIO_Pin=GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_9 | GPIO_Pin_10;
	GPIOA_Struct.GPIO_PuPd=GPIO_PuPd_NOPULL;
	GPIOA_Struct.GPIO_Speed=GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIOA_Struct);

	// LED Output
	GPIOA_Struct.GPIO_Mode=GPIO_Mode_OUT;
	GPIOA_Struct.GPIO_OType=GPIO_OType_PP;
	GPIOA_Struct.GPIO_Pin=GPIO_Pin_5 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_11 | GPIO_Pin_12;
	GPIO_Init(GPIOA, &GPIOA_Struct);
	
	GPIO_ResetBits(GPIOA, GPIO_Pin_7);
	GPIO_ResetBits(GPIOA, GPIO_Pin_8);
	GPIO_ResetBits(GPIOA, GPIO_Pin_11);
	GPIO_ResetBits(GPIOA, GPIO_Pin_12);

	// Button
	GPIOA_Struct.GPIO_Mode=GPIO_Mode_IN;
	GPIOA_Struct.GPIO_Pin=GPIO_Pin_13;
	GPIO_Init(GPIOC, &GPIOA_Struct);
	// AF on port C
	GPIOA_Struct.GPIO_Pin=GPIO_Pin_6 | GPIO_Pin_7;
	GPIOA_Struct.GPIO_Mode=GPIO_Mode_AF;
	GPIO_Init(GPIOC, &GPIOA_Struct);

	//USART
	USART_Struct.USART_BaudRate=9600;
	USART_Struct.USART_HardwareFlowControl=USART_HardwareFlowControl_None;
	USART_Struct.USART_Mode=USART_Mode_Tx | USART_Mode_Rx;
	USART_Struct.USART_Parity=USART_Parity_No;
	USART_Struct.USART_StopBits=USART_StopBits_1;
	USART_Struct.USART_WordLength=USART_WordLength_8b;
	
	// Enable USART's
	USART_Init(USART6, &USART_Struct);
	USART_Init(USART1, &USART_Struct);
	USART_ITConfig(USART6, USART_IT_RXNE, ENABLE);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
	USART_Cmd(USART6, ENABLE);

	//GPIO AF
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_USART6);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_USART6);


	//SYSCFG
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);

	//EXTI
	EXTI_Struct.EXTI_Line=EXTI_Line13;
	EXTI_Struct.EXTI_LineCmd=ENABLE;
	EXTI_Struct.EXTI_Mode=EXTI_Mode_Interrupt;
	EXTI_Struct.EXTI_Trigger=EXTI_Trigger_Falling;
	EXTI_Init(&EXTI_Struct);


	//NVIC
	NVIC_Struct.NVIC_IRQChannel=EXTI15_10_IRQn;
	NVIC_Struct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Struct.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_Struct.NVIC_IRQChannelSubPriority=0;
	NVIC_Init(&NVIC_Struct);
	NVIC_Struct.NVIC_IRQChannel= USART1_IRQn;
	NVIC_Init(&NVIC_Struct);
	NVIC_Struct.NVIC_IRQChannel=USART6_IRQn;
	NVIC_Init(&NVIC_Struct);
	GPIO_ResetBits(GPIOA, GPIO_Pin_5);
	
	while (1) {}
}


void EXTI15_10_IRQHandler(void){
	if(button_pressed == 0) {
		GPIO_SetBits(GPIOA, GPIO_Pin_7);
		wait_ms(100);
		GPIO_ResetBits(GPIOA, GPIO_Pin_7);
		wait_ms(100);
		USART_SendData(USART6, '0');
		button_pressed = 1;
	} else {
		change_direction = 1;
	}

	EXTI_ClearITPendingBit(EXTI_Line13);
}

void resetLEDs() {
	GPIO_ResetBits(GPIOA, GPIO_Pin_7);
	GPIO_ResetBits(GPIOA, GPIO_Pin_8);
	GPIO_ResetBits(GPIOA, GPIO_Pin_11);
	GPIO_ResetBits(GPIOA, GPIO_Pin_12);
}

void nextLED() {
	resetLEDs();
	GPIO_SetBits(GPIOA, LEDs[current_led]);
	current_led++;
	if (current_led > 3) current_led = 0;
}

void prevLED() {
	resetLEDs();
	GPIO_SetBits(GPIOA, LEDs[current_led]);
	current_led--;
	if (current_led < 0) current_led = 3;
}

void interruptHandler(USART_TypeDef *USART_Rcv, USART_TypeDef *USART_Send) {
	button_pressed = 1;
	
	if (USART_Rcv == USART1) {
		nextLED();
	} else {
		prevLED();
	}
	
	wait_ms(200);
	
	USART_TypeDef *USART_Next = USART_Send;
	if (change_direction == 1) {
		USART_Next = USART_Rcv;
		change_direction = 0;
	}
	
	USART_SendData(USART_Next, '0');
}

void USART6_IRQHandler(void) {
	interruptHandler(USART6, USART1);
	USART_ClearITPendingBit(USART6, USART_IT_RXNE);
}


void USART1_IRQHandler(void) {
	interruptHandler(USART1, USART6);
	USART_ClearITPendingBit(USART1, USART_IT_RXNE);
}

