#include "stm32f4xx_conf.h"
#include <string.h>

const char *numberMapping[10] = {
	"null",
	"eins",
	"zwei",
	"drei",
	"vier",
	"f�nf",
	"sechs",
	"sieben",
	"acht",
	"neun"
};

int main(void)
{
    // Prototypen:
    GPIO_InitTypeDef GPIO_InitStruct;
    USART_InitTypeDef USART_InitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;

    // Strom:
    RCC_AHB1PeriphClockCmd (RCC_AHB1Periph_GPIOA, ENABLE);
    RCC_APB1PeriphClockCmd (RCC_APB1Periph_USART2, ENABLE);
    RCC_APB2PeriphClockCmd (RCC_APB2Periph_USART1, ENABLE);

    // Config:
		// GPIO Pins fue USART2 (PA2, PA3) und USART1 (PA9, PA10) konfigurieren
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_Init (GPIOA, &GPIO_InitStruct);

		// USART2 und USART1 konfigurieren und initialisieren
    USART_InitStruct.USART_BaudRate = 9600;
    USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_InitStruct.USART_Parity = USART_Parity_No;
    USART_InitStruct.USART_StopBits = USART_StopBits_1;
    USART_InitStruct.USART_WordLength = USART_WordLength_8b;
    USART_Init (USART2, &USART_InitStruct);
    USART_Init (USART1, &USART_InitStruct);

		// Interrupts von der USART2 im NVIC zulassen
    NVIC_InitStruct.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
    NVIC_Init (&NVIC_InitStruct);

		// Interrupts von der USART1 im NVIC zulassen (Preemption Priority und Sub Priority werden noch aus dem Struct uebernommen)
    NVIC_InitStruct.NVIC_IRQChannel = USART1_IRQn;
    NVIC_Init (&NVIC_InitStruct);

    // Action:
		// Alternate Function von den GPIO Pins zu den USART's konfigurieren
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

		// Interrupts der USART1 und USART2 auf die ReceiveNotEmpty Flag aktivieren
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

		// USART1 und USART2 aktivieren
    USART_Cmd(USART1, ENABLE);
    USART_Cmd(USART2, ENABLE);

 while (1)
  {
  }
}

// Sende einen String ueber von der anzugebenden USARTx weg
void sendChars(USART_TypeDef *USARTx, const char text[]) {
	for(int i=0; i<strlen(text); i++) {
		while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) != SET){}; // l�uft in Schleife und checkt, ob der Transmit Buffer leer ist
    USART_SendData(USARTx, text[i]);
	}
}

// Empfange Daten vom PC (Hterm)
void USART2_IRQHandler (void){
    char data;
    data = USART_ReceiveData(USART2);
	
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) != SET); // l�uft in Schleife und checkt, ob der Transmit Buffer leer ist
    USART_SendData(USART1, data);
}

// Empfange Daten vom anderen Nucelo
void USART1_IRQHandler (void){
    char data;
    data = USART_ReceiveData(USART1);
		
		// Pruefen ob eine Zahl zwischen 0 und 9 gesendet wurde
		if (data >= 48 && data <= 57) {
				sendChars(USART2, *(numberMapping+(data-48)));
		} else {
				while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) != SET); // l�uft in Schleife und checkt, ob der Transmit Buffer leer ist
				USART_SendData(USART2, data);
		}
}
