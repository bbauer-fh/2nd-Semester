#include "stm32f4xx_conf.h"

void wait_ms(int ms) {
    int i;
    for (i=1; i<ms*16000; i++);    
}

int main(void) {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM10, ENABLE);

	GPIO_InitTypeDef GPIOA_Struct;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	NVIC_InitTypeDef NVIC_Struct;

	// LED Output
	GPIOA_Struct.GPIO_Mode=GPIO_Mode_OUT;
	GPIOA_Struct.GPIO_OType=GPIO_OType_PP;
	GPIOA_Struct.GPIO_Pin=GPIO_Pin_5;
	GPIOA_Struct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIOA_Struct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIOA_Struct);
	
	// Timer
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Period = 999;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 3999;
	TIM_TimeBaseInit(TIM10, &TIM_TimeBaseInitStruct);
	TIM_ITConfig(TIM10, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM10, ENABLE);

	//NVIC
	NVIC_Struct.NVIC_IRQChannel=TIM1_UP_TIM10_IRQn;
	NVIC_Struct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Struct.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_Struct.NVIC_IRQChannelSubPriority=0;
	NVIC_Init(&NVIC_Struct);
	
	while (1) {}
}


void TIM1_UP_TIM10_IRQHandler(void){
	GPIO_SetBits(GPIOA, GPIO_Pin_5);
	for(int i=0; i<3000; i++);
	GPIO_ResetBits(GPIOA, GPIO_Pin_5);
	TIM_ClearITPendingBit(TIM10, TIM_IT_Update);
}
