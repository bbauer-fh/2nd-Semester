/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_conf.h"

void wait_ms(int ms) {
	// warte ein bisschen
	int i;
	for (i=1; i<ms*20000; i++);
}

int main(void) {
//	// GPIOA aktivieren
//	*((unsigned long*)0x40023830) |= (1 << 0);
//	
//	// PA11 als Output konfigurieren
//	*((unsigned long*)0x40020000) |= (1 << 22);
//	// PA11 einschalten
//	*((unsigned long*)0x40020014) |= (1 << 11);
	
	GPIO_InitTypeDef GPIO_InitStruct;
	TIM_TimeBaseInitTypeDef TIM_InitStruct;
	TIM_OCInitTypeDef TIM_OCInitStruct;
	
	// Bestromen
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	// Init GPIO pin
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// AF mapping setzen
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_TIM3);
	
	// Timer initialisieren
	TIM_InitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_InitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_InitStruct.TIM_Period = 999;
	TIM_InitStruct.TIM_Prescaler = 7999;
	TIM_TimeBaseInit(TIM3, &TIM_InitStruct);
	
	// Timer OC register f�r pwm beschalten
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse = 100;
	TIM_OC2Init(TIM3, &TIM_OCInitStruct);
	
	
	TIM_Cmd(TIM3, ENABLE);
	
	while (1) {}
}
