/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_conf.h"

int main(void) {

	// Die Struktur brauchma, um irgendeine ...init aufzurufen...
	GPIO_InitTypeDef GPIO_InitStruct;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
//	NVIC_InitTypeDef NVIC_InitStruct;
	TIM_OCInitTypeDef TIM_OCInitStruct;
	
	// Wir wollen nun die LED zum Leuchten bringen,
	// aber unter Verwendung der FW-Library ... Muhahaha!
	
	// 1. Schritt, GPIO aktivieren	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	// Timer 2 aktivieren
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	
	// 2. Schritt, Pin5 Initialisierung = ALTERNATE FUNCTION
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	// Wenn AF, dann muss auch ausgew�hlt werden, welche es sein soll
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_TIM2);

	
	// 3. Schritt, LED einschalten
	GPIO_SetBits(GPIOA, GPIO_Pin_5);
	
	// auf geht's: Timer
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Period = 999;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 7999; // runter auf 1Mhz
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStruct);
	
	// Jetzt nich den Timer von der Kette lassen...
	TIM_Cmd(TIM2, ENABLE);
	
	// Jetzt m�ssen wir dem Timer noch sagen
	// dass er mit seinem OC1 ein PWM-Signal erzeugen soll
	//TIM_OCInitStruct.TIM_OCIdleState = ;
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	//TIM_OCInitStruct.TIM_OCNIdleState = ...; // Nur f�r Timer 1 (Advanced)
	//TIM_OCInitStruct.TIM_OCNPolarity = ...; // Nur f�r Timer 1 (Advanced)
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	//TIM_OCInitStruct.TIM_OutputNState = ...; // Nur f�r Timer 1 (Advanced)
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse = 500; // "VERGLEICHS"-Wert
	TIM_OC1Init(TIM2, &TIM_OCInitStruct);
	
	// Jetzt machen wir uns daran den NVIC zu konfigurieren
	// das klingt schlimmer, als es ist
//	NVIC_InitStruct.NVIC_IRQChannel = TIM2_IRQn;
//	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
//	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
//	NVIC_Init(&NVIC_InitStruct);
	
	
	
	while (1) {}
}
