/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_conf.h"

int main(void) {
	// Init Strukturen
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;
	
	// wir bestromen, was zu bestromen ist
	//************************************
	// GPIOA bestromen
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	// USART2 bestromen
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	
	// GPIO konfigurieren
	// A.2 (TX) ... AF !!
	// A.3 (RX) ... AF !!
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	// und schwupp - zur Init-Funktion
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	//AF auf USART2 einstellen
	// wir muessen da gar nicht einmal die Nummer der AF herausfinden
	// die FW-Lib versorft uns mit einer Konstanten im Klartext "USART2"
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);
	
	// USART2 konfigurieren
	USART_InitStruct.USART_BaudRate = 9600;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	// uns schwupp - zur Init-Funktion
	USART_Init(USART2, &USART_InitStruct);
	
	// USART jetzt noch "einschalten"
	USART_Cmd(USART2, ENABLE);
	
	// USART darf einen bestimmten Interrupt (RXNE) ausloesen
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	
	NVIC_InitStruct.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
	
	
	// zur Sicherheit warten wir hier, bis der Sendepuffer auch wirklich leer ist...
	// indem wir warten, bis das TXE (Transmission Buffer EMPTY)-Flag gesetzt ist
	while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) != SET);

	// ..dann Feuer frei!
	USART_SendData(USART2, 'A');
	
	// Noch ein byte senden
	while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) != SET);
	USART_SendData(USART2, 'B');
	
	while (1) {}
}

void USART2_IRQHandler(void) {
	// Daten von USART2 auslesen
	char receive_data = USART_ReceiveData(USART2);
	
	// Empfangene Daten wieder zurücksenden
	while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) != SET);
	USART_SendData(USART2, receive_data);
	
	//Sollte nicht verwendet werden bei RXNE
	//USART_ClearITPendingBit(USART2, USART_IT_RXNE);
}
