import { Component } from '@angular/core';
import { utf8Encode } from '@angular/compiler/src/util';
import { sha256 } from 'js-sha256';
import * as faker from 'faker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  hashsum: String = 'Hash will generate automatically on input change.';
  color: String = '#FF6347';
  randomGeneratorOutput = 'Press one of the buttons above to generate some random solutions!';

  hostname: String = '';
  username: String = 'root';
  owner: String = 'root';
  group: String = '';
  permissions: String = '0o4750';
  swap: Number = 4; // 4194300
  multithreading: String = '';
  sorted: Number = 6;

  onInputChange = () => {
    this.hashsum = this.generateSolutionsOutput(
      this.hostname,
      this.username,
      this.owner,
      this.group,
      this.permissions,
      this.swap,
      this.multithreading,
      this.sorted
    );
    this.color = '#7FFF00';
  }

  onGenerateALot(num: number) {
    this.randomGeneratorOutput = '';

    for (let i = 0; i < num; i++) {
      if (this.randomGeneratorOutput !== '') {
        this.randomGeneratorOutput += '<br />====================================================================================<br />';
      }
      const group = faker.name.firstName().toLowerCase();
      this.randomGeneratorOutput += '\n\n';
      this.randomGeneratorOutput += this.generateSolutionsOutput(
        this.pad(Math.round(Math.random() * 2000000000), 10),
        'root',
        'root',
        group,
        `0o${Math.round(Math.random() * 7).toString()}${Math.round(Math.random() * 7).toString()}${Math.round(Math.random() * 7).toString()}`,
        Math.round(Math.random() * 10),
        (Math.round(Math.random()) ? true : false),
        Math.round(Math.random() * 6),
      );
    }
  }

  generateSolutionsOutput(hostname, username, owner, group, permissions, swap, multithreading, sorted) {
    let solution = 'hostname: ' + hostname + '\n';
    solution += 'username: ' + username + '\n';
    solution += 'owner: ' + owner + '\n';
    solution += 'group: ' + group + '\n';
    solution += 'permissions: ' + permissions + '\n';
    solution += 'Swap: ' + swap + '\n';
    solution += (multithreading ? 'multithreading is active' : 'multithreading is NOT active') + '\n';
    solution += 'sorted: ' + sorted.toString() + '\n';

    let hashsum = solution.replace(/\n/g, '<br />');

    solution = '@^@^@test@^@^@' + solution + '@^@^@test@^@^@';
    hashsum += '<br />' + sha256(utf8Encode(solution));

    return hashsum;
  }

  pad(num, size) {
    let s = num + '';
    while (s.length < size) {
      s = '0' + s;
    }
    return s;
  }

}
