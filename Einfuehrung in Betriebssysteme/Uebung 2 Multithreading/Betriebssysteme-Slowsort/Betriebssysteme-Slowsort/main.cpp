//
//  main.cpp
//  Betriebssysteme-Slowsort
//
//  Created by Bernhard Bauer on 24.05.18.
//  Copyright © 2018 Bernhard Bauer. All rights reserved.
//

#include <iostream>
#include <thread>

void slowsort(int *A, int i, int j) {
    if (i >= j) return;
    
    int m = ((i+j)/2);
    slowsort(A, i, m);
    slowsort(A, m+1, j);
    
    if (A[j] < A[m]) {
        int tmp = A[j];
        A[j] = A[m];
        A[m] = tmp;
    }
    slowsort(A, i, j-1);
}

void print(int *A, int size) {
    for (int i=0; i<size; i++) {
        std::cout << A[i] << ", ";
    }
}

int main(int argc, const char * argv[]) {
    int numbers1[75];
    int numbers2[75];
    int numbers3[75];
    int numbers4[75];
    for (int i=0; i<75; i++) {
        numbers1[i] = 74-i;
        numbers2[i] = 149-i;
        numbers3[i] = 224-i;
        numbers4[i] = 299-i;
    }

    std::cout << "=> Unsortiert:" << std::endl;
    print(numbers1, 75);
    print(numbers2, 75);
    print(numbers3, 75);
    print(numbers4, 75);
    std::cout << std::endl;
    
    std::thread t1(slowsort, numbers1, 0, 74);
    std::thread t2(slowsort, numbers2, 0, 74);
    std::thread t3(slowsort, numbers3, 0, 74);
    std::thread t4(slowsort, numbers4, 0, 74);

    t1.join();
    t2.join();
    t3.join();
    t4.join();
    
    std::cout << "=> Sortiert:" << std::endl;
    print(numbers1, 75);
    print(numbers2, 75);
    print(numbers3, 75);
    print(numbers4, 75);
    
    return 0;
}
