class node {
	name = "N/A";
	color = 0;
	startTime = 0;
	endTime = 0;
}

export default class Graph {

	MAX_NODES = 20;
	WHITE = 0;
	GRAY = 1;
	BLACK = 2;

	nodes = [];
	adjMatrix; // int
	time = 0;

	constructor () {
		this.adjMatrix = Array.from(Array(this.MAX_NODES), () => new Array(this.MAX_NODES));

		this.reset();
	}

	reset = () => {
		for (let i = 0; i < this.MAX_NODES; i++) {
			for (let j = 0; j < this.MAX_NODES; j++) {
				this.adjMatrix[i][j] = 0;
			}
		}

		this.nodes = [];
		this.time = 0;
	}

	addNode(name) {
		for (let i = 0; i < this.nodes.length; i++) {
			// if we find a node with the name return the index
			if (this.nodes[i].name === name) {
				return i;
			}
		}

		let n = new node();
		n.name = name;
		this.nodes.push(n);

		return this.nodes.length - 1;
	}

	// DFS Search
	toplogySearchUtil = (source) => {
		this.nodes[source].color = this.GRAY;
		this.nodes[source].startTime = ++this.time;

		for (let i = 0; i < this.nodes.length; i++) {
			if (this.adjMatrix[i][source]) {
				if (this.nodes[i].color === this.WHITE) {
					this.toplogySearchUtil(i);
				}
			}
		}

		this.nodes[source].color = this.BLACK;
		this.nodes[source].endTime = ++this.time;
	}

	topologySearch = () => {
		for (let i = 0; i < this.nodes.length; i++) {
			if (this.nodes[i].color === this.WHITE) {
				this.toplogySearchUtil(i);
			}
		}

		// Duplicate the nodes array and sort it by endtime and return it
		return Array.from(this.nodes).sort((a, b) => {
			return a.endTime < b.endTime;
		});
	}

	getReactD3Object() {
		let linkList = [];
		this.adjMatrix.forEach((dependencies, dependantId) => {
			// console.log("======================================================");
			// console.log(dependantId, dependencies);
			dependencies.forEach((linkActive, index) => {
				//console.log("==> ", index, linkActive);
				if (linkActive === 1) {
					linkList.push({ source: this.nodes[index].name, target: this.nodes[dependantId].name });
				}
			});
		})

		return {
			nodes: this.nodes.map((node, index) => {
				return { id: node.name };
			}),
			links: linkList
		};
	}

}
