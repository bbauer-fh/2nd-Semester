import React, { Component } from 'react';
import { Graph as D3Graph } from 'react-d3-graph';

import './App.scss';
import Graph from './graph';

class App extends Component {

	graph = new Graph();
	topologyTree = null;
	state = {
		compilationOrder: [],
		data: {
			nodes: [{ id: 'Harry' }, { id: 'Sally' }, { id: 'Alice' }],
			links: [{ source: 'Harry', target: 'Sally' }, { source: 'Harry', target: 'Alice' }]
		},
		config: {
			automaticRearrangeAfterDropNode: true,
			nodeHighlightBehavior: false,
			height: 1000,
			node: {
				color: 'white',
				"fontColor": "white",
				"fontSize": 16,
				size: 250,
				highlightStrokeColor: 'blue'
			},
			link: {
				highlightColor: 'lightblue'
			}}
	};

	componentDidMount() {
		const browserWidth = Math.max(
			document.body.scrollWidth,
			document.documentElement.scrollWidth,
			document.body.offsetWidth,
			document.documentElement.offsetWidth,
			document.documentElement.clientWidth
		);

		if (this.state.config.width !== browserWidth) {
			let newConfig = this.state.config;
			newConfig.width = browserWidth;
			this.setState({config: newConfig});
		}
	}

	onFileUpload = (event) => {
		console.log(event.target);
		var file = event.target.files[0];
		if (file) {
			var reader = new FileReader();
			reader.readAsText(file, "UTF-8");
			reader.onload = (evt) => {
				// Read the file content into a variable
				let content = evt.target.result;
				 // Split by lines
				content = content.split('\n');

				// reset the graph
				this.graph.reset();

				// Loop over lines
				content.forEach((value) => {
					if (value !== "") {
						value = value.split(" : ");

						// add the node to the graph
						let target = this.graph.addNode(value[0]);

						value[1].split(" ").forEach(dependency => {
							let source = this.graph.addNode(dependency);
							// add an edge to the adjacency matrix
							this.graph.adjMatrix[target][source] = 1;
							console.log("["+target+", "+this.graph.nodes[target].name+"] <- ["+source+", "+this.graph.nodes[source].name+"]");
						});
					}
				});

				console.log(this.graph);

				// DFS search
				const compilationOrder = this.graph.topologySearch();

				// Redraw the graph and add the compilaton order
				this.setState({
					data: this.graph.getReactD3Object(),
					compilationOrder: compilationOrder
				});
			}
			reader.onerror = function (evt) {
				console.log("error reading file");
			}
		}
	}

	render() {
		return (
			<div className="App">
				<div className="insertPane">
					<h1>Topology Search</h1>

					<div className="inputWrapper">
						<input type="file" id="file" onChange={ this.onFileUpload }  onClick={(event)=> { event.target.value = null }} />
						<label htmlFor="file">Choose a file ...</label>
					</div>
				</div>

				<div className="treeWrapper">
					<D3Graph
						id="graph-id" // id is mandatory, if no id is defined rd3g will throw an error
						data={this.state.data}
						config={this.state.config}
						/>
				</div>
				<div className="compilationOrderOverlay">
					<h4>Compilation Order</h4>
					{ this.state.compilationOrder.map((e, idx) => {
						return (<p key={ idx+1 }>{ idx+1 } - { e.name } (S: { e.startTime }, E: { e.endTime })</p>);
					}) }
				</div>
			</div>
		);
	}
}

export default App;
