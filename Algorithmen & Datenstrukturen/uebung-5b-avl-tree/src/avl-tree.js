
export default class AVLTree {

	constructor() {
		// Binary Tree
		this.root = null;
		this.size = 0;
	}

	// Insert a node into the binary tree
	// hopNode is for recursive insertion (internally used)
	// Returns true if the node has been inserted, otherwise false
	insert = (node, hopNode) => {
		if (this.root === null) {
			this.root = node;
			return true;
		} else {
			if (hopNode === undefined) {
				hopNode = this.root;
			}

			if (node.key < hopNode.key) {
				if (hopNode.left === null) {
					hopNode.left = node;
					this.balanceTree();
					return true;
				} else {
					return this.insert(node, hopNode.left);
				}
			} else if (node.key > hopNode.key) {
				if (hopNode.right === null) {
					hopNode.right = node;
					this.balanceTree();
					return true;
				} else {
					return this.insert(node, hopNode.right);
				}
			} else {
				console.log("Could not insert new Node. Id already taken.");
				return false;
			}
		}

		return false;
	}

	// node = internal
	lookup = (key, node) => {
		if (node === undefined) {
			node = this.root;
		}

		if (node !== null) {
			if (node.key === key) {
				return node;
			} else {
				if (key < node.key) {
					return this.lookup(key, node.left);
				} else if (key > node.key) {
					return this.lookup(key, node.right);
				}
			}
		}

		return null; // Node
	}

	// Find the balance of an AVL node
	balanceFactor = (node) => {
		let bf = 0;
		if(node.left) bf += this.treeDepth(node.left);
		if(node.right) bf -= this.treeDepth(node.right);
		return bf;
	}

	// Balance a given node
	balanceNode = (node) => {
		let newroot = null;

		// Balance our children, if they exist.
		if(node.left) node.left = this.balanceNode(node.left);
		if(node.right) node.right = this.balanceNode(node.right);

		const bf = this.balanceFactor(node);

		if (bf >= 2) {
			/* Left Heavy */
			if (this.balanceFactor(node.left) <= -1) {
				newroot = this.rotationLR(node);
			} else {
				newroot = this.rotationLL(node);
			}
		} else if (bf <= -2) {
			/* Right Heavy */
			if (this.balanceFactor(node.right) >= 1) {
				newroot = this.rotationRL(node);
			} else {
				newroot = this.rotationRR(node);
			}
		} else {
			/* This node is balanced -- no change. */
			newroot = node;
		}

		return newroot;
	}

	balanceTree = () => {
		const newroot = this.balanceNode(this.root);

		if(newroot !== this.root)  {
			this.root = newroot;
		}
	}

	rotationLL = (node) => {
		console.log("rotate LL");
		let a = node;
		let b = a.left;

		a.left = b.right;
		b.right = a;

		return b;
	}

	rotationRR = (node) => {
		console.log("rotate RR");
		let a = node;
		let b = a.right;

		a.right = b.left;
		b.left = a;

		return b;
	}

	rotationLR = (node) => {
		console.log("rotate LR");
		let a = node;
		let b = a.left;
		let c = b.right;

		a.left = c.right;
		b.right = c.left;
		c.left = b
		c.right = a;

		return c;
	}

	rotationRL = (node) => {
		console.log("rotate RL");
		let a = node;
		let b = a.right;
		let c = b.left;

		a.right = c.left;
		b.left = c.right;
		c.right = b
		c.left = a;

		return c;
	}

	treeDepth = (node) => {
		if (this.root !== null && node === undefined) {
			node = this.root;
		}

		if (node !== undefined && node !== null) {
			let lDepth = this.treeDepth(node.left);
			let rDepth = this.treeDepth(node.right);

			if (lDepth > rDepth)  {
				return(lDepth+1);
			} else {
				return(rDepth+1);
			}
		}

		return 0;
	}

	getReactD3TreeObject = (node) => {
		if (this.root !== null) {
			if (node === undefined) {
				return [{
					name: this.root.key + ": " + this.root.data + " (root)",
					children: this.getReactD3TreeObject(this.root)
				}];
			} else if (node !== null) {
				let children = [];

				if (node.left !== null) {
					children.push({
						name: node.left.key + ": " + node.left.data,
						children: this.getReactD3TreeObject(node.left)
					});
				}
				if (node.right !== null) {
					children.push({
						name: node.right.key + ": " + node.right.data,
						children: this.getReactD3TreeObject(node.right)
					});
				}

				return children;
			}
		}

		return null;
	}

}
