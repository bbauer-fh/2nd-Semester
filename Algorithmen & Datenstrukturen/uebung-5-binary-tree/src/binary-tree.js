
export default class BinaryTree {

	constructor() {
		// Binary Tree
		this.root = null;
		this.size = 0;

	}

	// Insert a node into the binary tree
	// hopNode is for recursive insertion (internally used)
	// Returns true if the node has been inserted, otherwise false
	insert = (node, hopNode) => {
		if (this.root === null) {
			this.root = node;
			return true;
		} else {
			if (hopNode === undefined) {
				hopNode = this.root;
			}

			if (node.key < hopNode.key) {
				if (hopNode.left === null) {
					hopNode.left = node;
					return true;
				} else {
					return this.insert(node, hopNode.left);
				}
			} else if (node.key > hopNode.key) {
				if (hopNode.right === null) {
					hopNode.right = node;
					return true;
				} else {
					return this.insert(node, hopNode.right);
				}
			} else {
				console.log("Could not insert new Node. Id already taken.");
				return false;
			}
		}

		return false;
	}

	// node = internal
	lookup = (key, node) => {
		if (node === undefined) {
			node = this.root;
		}

		if (node !== null) {
			if (node.key === key) {
				return node;
			} else {
				if (key < node.key) {
					return this.lookup(key, node.left);
				} else if (key > node.key) {
					return this.lookup(key, node.right);
				}
			}
		}

		return null; // Node
	}

	treeDepth = (node) => {
		if (this.root !== null && node === undefined) {
			node = this.root;
		}

		if (node !== undefined && node !== null) {
			let lDepth = this.treeDepth(node.left);
			let rDepth = this.treeDepth(node.right);

			if (lDepth > rDepth)  {
				return(lDepth+1);
			} else {
				return(rDepth+1);
			}
		}

		return 0;
	}

	getReactD3TreeObject = (node) => {
		if (this.root !== null) {
			if (node === undefined) {
				return [{
					name: this.root.key + ": " + this.root.data + " (root)",
					children: this.getReactD3TreeObject(this.root)
				}];
			} else if (node !== null) {
				let children = [];

				if (node.left !== null) {
					children.push({
						name: node.left.key + ": " + node.left.data,
						children: this.getReactD3TreeObject(node.left)
					});
				}
				if (node.right !== null) {
					children.push({
						name: node.right.key + ": " + node.right.data,
						children: this.getReactD3TreeObject(node.right)
					});
				}

				return children;
			}
		}

		return null;
	}

}
