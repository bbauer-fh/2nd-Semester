import Node from './Node.js'

export default class LinkedList {
	constructor() {
		this.count = 0;
		this.head = null;
		this.tail = null;
	}

	insertBefore(key, data) {
		if (this.head === null) {
			return this.insertEnd(key, data);
		}

		// Create new node for insertBefore
		const newNode = new Node(
			null,
			null,
			key,
			data
		);

		let elem = this.head;
		while (elem !== null) {
			// Assumes that the elemens are ordered by key!
			if (elem.key >= key) {
				// If elem is somewhere in the middle of the list
				if (elem.previous !== null && elem.next !== null) {
					newNode.next = elem;
					newNode.previous = elem.previous;
					elem.previous = newNode;
					newNode.previous.next = newNode;
					this.count++;
					return true;
				// Is tail element -> set previous to tail
				} else if (elem.previous !== null && elem.next === null) {
					newNode.next = elem;
					newNode.previous = elem.previous;
					elem.previous = newNode;
					this.count++;
					return true;
				// Is head element -> set next to head if available
				} else if (elem.previous === null && elem.next !== null) {
					newNode.next = elem;
					elem.previous = newNode;
					this.head = newNode;
					this.count++;
					return true;
				}
				elem = null;
			} else if (elem.next === null) {
				this.insertEnd(key, data);
				elem = null;
			} else {
				elem = elem.next;
			}
		}
	}

	// Elements should be ordered by key
	insertEnd(key, data) {
		const newNode = new Node(
			null,
			null,
			key,
			data
		);

		if (this.tail !== null) {
			this.tail.next = newNode;
			newNode.previous = this.tail;
			this.tail = newNode;
			this.count++;
			return true;
		} else if (this.head !== null) {
			this.head.next = newNode;
			newNode.previous = this.head;
			this.tail = newNode;
			this.count++;
			return true;
		} else {
			this.head = newNode;
			this.count++;
			return true;
		}
	}

	delete(key) {
		let deleteableNode = this.findNode(key);
		if (deleteableNode !== null) {
			// If elem is somewhere in the middle of the list
			if (deleteableNode.previous !== null && deleteableNode.next !== null) {
				deleteableNode.previous.next = deleteableNode.next;
				deleteableNode.next.previous = deleteableNode.previous;
				this.count--;
				return true;
			// Is tail element -> set previous to tail
			} else if (deleteableNode.previous !== null && deleteableNode.next === null) {
				deleteableNode.previous.next = null;
				this.tail = deleteableNode.previous;
				this.count--;
				return true;
			// Is head element -> set next to head if available
			} else if (deleteableNode.previous === null && deleteableNode.next !== null) {
				deleteableNode.next.previous = null;
				this.head = deleteableNode.next;
				this.count--;
				return true;
			}
		}
		return false;
	}

	findNode(key, current = null) {
		if (current !== null) {
			if (current.key === key) {
				return current;
			} else if (current.next !== null) {
				return this.findNode(key, current.next);
			}
		} else if (this.head !== null) {
			return this.findNode(key, this.head);
		}

		return null;
	}
}
