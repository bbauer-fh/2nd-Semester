import React, { Component } from 'react';
import './App.css';
import RecursiveH from './Containers/RecursiveH'
import Linienalgorithmus from './Containers/Linienalgorithmus'

class App extends Component {
	render() {
		return (
			<div className="App">
				<RecursiveH />
				<Linienalgorithmus />
			</div>
		);
	}
}

export default App;
